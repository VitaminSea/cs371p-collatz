// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <unordered_map>

#include "Collatz.h"

using namespace std;


std::unordered_map<long, int> cache({
    { 1, 1 },
    { 2, 2 }, {129991, 344}, {259982, 345}, {173321, 347}, {115547, 349}, {77031, 351}
});

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

//optimization 1!
long optim1(long i, long j) {
    if(i < ((j / 2) + 1)) {
        return (j / 2) + 1;
    }
    return i;
}

//recursively finds the cycle length for a number using the collatz algorithm
int collatz_rec (long curr, int currCycle, int val) {
    if(cache.count(curr)) {
        return cache.find(curr)->second + currCycle;
    }

    if(curr % 2 == 0) {
        int endCycle = collatz_rec(curr / 2, currCycle + 1, val);
        cache[curr] = endCycle - currCycle;
        return endCycle;
    } else {
        //optimization 2! Odd case, 2 steps in one!
        int endCycle = collatz_rec(curr + (curr >> 1) + 1, currCycle + 2, val);
        cache[curr] = endCycle - currCycle;
        return endCycle;
    }
}


// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    //assert(i != j);
    if(i > j) {
        i = i + j;
        j = i - j;
        i = i - j;
    }

    i = optim1(i, j);

    long maxCycle = 0;

    for(; i <= j; i++) {
        long curr = (long) i;
        long currCycle = 0;

        currCycle = collatz_rec(curr, currCycle, curr);

        if(currCycle > maxCycle) {
            maxCycle = currCycle;
        }
    }

    return maxCycle;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
