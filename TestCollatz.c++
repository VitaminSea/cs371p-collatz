// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

// -----
// optim1
// -----

TEST(CollatzFixture, eval_11) {
    const int v = optim1(900, 1000);
    ASSERT_EQ(v, 900);
}

TEST(CollatzFixture, eval_12) {
    const int v = optim1(501, 1000);
    ASSERT_EQ(v, 501);
}

TEST(CollatzFixture, eval_13) {
    const int v = optim1(1, 1000);
    ASSERT_EQ(v, 501);
}

// -----
// collatz_rec
// -----

TEST(CollatzFixture, eval_14) {
    const int v = collatz_rec(1, 0, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval_15) {
    const int v = collatz_rec(2, 0, 2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, eval_16) {
    const int v = collatz_rec(3, 0, 3);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, eval_17) {
    const int v = collatz_rec(1000000, 0, 1000000);
    ASSERT_EQ(v, 153);
}

TEST(CollatzFixture, eval_18) {
    const int v = collatz_rec(2000000, 0, 2000000);
    ASSERT_EQ(v, 154);
}

// ----
// eval - my unit tests
// ----

TEST(CollatzFixture, eval_19) {
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval_20) {
    const int v = collatz_eval(1, 1000000);
    ASSERT_EQ(v, 525);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
